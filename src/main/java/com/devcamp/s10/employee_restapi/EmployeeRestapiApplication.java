package com.devcamp.s10.employee_restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeRestapiApplication.class, args);
	}

}
