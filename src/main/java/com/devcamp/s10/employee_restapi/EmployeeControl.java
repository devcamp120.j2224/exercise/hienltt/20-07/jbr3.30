package com.devcamp.s10.employee_restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeControl {
    @CrossOrigin
    @GetMapping("/employees")
    public ArrayList<Employee> listEmloyee(){

        Employee employee1 = new Employee(1, "Nguyen Van", "A", 10000000);
        Employee employee2 = new Employee(2, "Tran Van", "B", 12000000);
        Employee employee3 = new Employee(3, "Pham Van", "C", 14000000);
        System.out.println(employee1);
        System.out.println(employee2);
        System.out.println(employee3);

        ArrayList<Employee> listEmployee = new ArrayList<>();
        listEmployee.add(employee1);
        listEmployee.add(employee2);
        listEmployee.add(employee3);
        return listEmployee;
    }
}
